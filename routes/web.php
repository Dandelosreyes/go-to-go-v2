<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/','login');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('menu/{category}','HomeController@menu')->name('menu');
Route::get('maps/{id?}','HomeController@map')->name('maps');
Route::post('feedback/{store?}','HomeController@feedback')->name('feedback');

Route::view('messages','messages',['title' => 'Messages'])->name('messages');
Route::view('profile','profile',['title' => 'My Profile'])->name('profile');
Route::view('settings','settings',['title' => 'My Settings'])->name('settings');
Route::view('favorites','favorites',['title' => 'My Favorites'])->name('favorites');
Route::view('aboutus','aboutus',['title' => 'About Go to Go'])->name('aboutus');
Route::post('reg','HomeController@register')->name('register.modified');