@extends('layouts.app')
@section('content')
<div class="container-modify">
    <div style="width:100%;min-height:400px;height: 100%;display:none;" id="map_canvas"></div>
    <div class="collection mt-1" style="background-color: white;" id="searchBox">
        <div class="container">
            <input type="text"    v-model="search" style="border:5;box-sizing: border-box;" placeholder="Store name or tags">
            <input type="number" v-model="price" name="quantity" min="0" max="100" placeholder="Ideal budget for your date <3">
        </div>
    </div>
    <blockquote id="blckqout">
        <h5> Recommended Stores for you..</h5>
    </blockquote>
    <ul class="collection">
        <li class="collection-item avatar" v-for="data in filteredStores">
          <i alt="" class="circle material-icons red">local_dining</i>
          <span class="title">@{{data.name}}</span>
          <p class="center-left flow-text">
              Description: @{{ data.tags }}
              <br>
              Location: @{{ data.address }} @{{ data.id }}
          </p>
          <a href="#"  class="secondary-content" v-on:click="test(data.id)"><i class="material-icons">directions</i></a>
        </li>
    </ul> 
</div>
@endsection
@push('scripts')
<script src="https://unpkg.com/vue"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script>
    var app = new Vue(
	{
		el: "#app",
		data:function()
		{
	        return {
	            data: {!! $data !!},
	        	search:null || '',
	        	price:null || '',
	        }
		},
		methods:{
		    test:function(id)
		    {
		        console.log(id);
		        window.location.assign("{{ route('maps') }}/"+id)
		    }
		},
		computed:
		{
			filteredStores:function()
			{
				return this.data.filter((data) => {
		  			return (data.name.toLowerCase().indexOf( this.search.toLowerCase() ) >=0 || data.tags.toLowerCase().indexOf( this.search.toLowerCase() ) >=0  && data.startprice > this.price);
		  		});
			}
		}
	}
);
</script>
@endpush