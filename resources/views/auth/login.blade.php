@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col s12 mt-3">
            <div class="box-center">
                <img class="materialboxed" width="200" src="img/logo.png">
            </div>
        </div>
    </div>
    <form class="col s12"  method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
      <div class="row txtbox-login">
        <div class="input-field col s12">
            <input type="text" name="email" class="user" placeholder="USERNAME">
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong style="color:red">{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
        <div class="input-field col s12">
            <input type="password"  name="password" class="pass" placeholder="PASSWORD">
        </div>
      </div>
    <div class="row">
        <div class="col s6">
            <img src="{{ asset('img/check.png') }}" width="25" align="left"><span class="loginHelpers">Remember Me</span>
        </div>
        <div class="col s6">
            <img src="{{ asset('img/forgot.png') }}" width="25" align="left"><span class="loginHelpers">Can't Log in?</span>
        </div>
    </div>
    <div class="row">
        <div class="col s12">
            <button class="btn-block btn yellow">LOG IN</a>
        </div>
    </div>
    <div class="row">
        <div class="col s12">
            <a href="{{ route('register') }}" class="btn-block btn red">SIGN UP</a>
        </div>
    </div>
        </form>

</div>
@endsection
