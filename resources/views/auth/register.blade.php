@extends('layouts.app')

@section('content')
	<div class="form-container">
		<h3 class="teal-text">Register</h3>
        <form class="form-horizontal" method="POST" route="{{ route('register') }}">
            {{ csrf_field() }}
            {{ method_field('POST') }}
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li style="color:red;">{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
		<div class="row">
			<div class="input-field col s12">
				<input id="text" type="text" name="name" class="validate" value="{{ old('name') }}" autofocus>
				<label for="name">Name</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input id="email" type="email" name="email" class="validate" value="{{ old('email') }}">
				<label for="email">Email</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input id="password" type="password"  name="password" class="validate">
				<label for="password">Password</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input id="password_confirmation" name="password_confirmation" type="password" class="validate">
				<label for="password_confirmation">Password Confirmation</label>
			</div>
		</div>
		<center>
			<button class="btn waves-effect waves-light yellow btn-block" type="submit">Submit</button>
			<br>
			<a href="{{ route('login') }}" class="btn waves-effect waves-light red btn-block">Login</a>
		</center>
	    </form>
	</div>
</div>
@endsection
@push('styles')
<style>
    body
{
	background: #f5f5f5;
}

h5
{
	font-weight: 400;
}

.container
{
	margin-top: 80px;
	width: 400px;
	height: 700px;
}

.tabs .indicator
{
	background-color: #e0f2f1;
	height: 60px;
	opacity: 0.3;
}

.form-container
{
	padding: 40px;
	padding-top: 10px;
}

.confirmation-tabs-btn
{
	position: absolute;
}
</style>
@endpush