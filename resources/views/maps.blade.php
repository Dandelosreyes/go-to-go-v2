@extends('layouts.app')
@section('content')
<div class="container-modify" id="app">
    <div style="width:100%;min-height:100%;height: 400px;margin-top:1em">
        {!! Mapper::render() !!}
    </div>
    <div class="col s12 m5" id="storeinfo">
        <div class="card-panel white">
          <h5 id="restname">{{ $store->name }}</h5>
          <span class="dark-text flow-text">
              <p>{{ $store->tags }}</p>
              <p>Starting price:{{ $store->startprice }}</p>
              <p>Ending price:{{ $store->startprice }}</p>
          </span>
            <div class="fixed-action-btn horizontal click-to-toggle">
                <a class="btn-floating btn-large red">
                  <i class="material-icons">menu</i>
                </a>
                <ul>
                  <li><a class="btn-floating blue"  v-on:click="back();"><i class="material-icons">arrow_back</i></a></li>
                  <li><a class="btn-floating yellow darken-1"><i class="material-icons">favorite</i></a></li>
                  <li><a class="waves-effect waves-light btn-floating modal-trigger" href="#modal1"><i class="material-icons"  v-on:click="submit();">feedback</i></a></li>
                </ul>
              </div>
        </div>
        <span class="dark-text flow-text">
          <ul class="collection">
          <h5 id="restname" style="margin-left:5%">Reviews and Feedbacks</h5>
            @foreach($feedbacks as $feedback)
               <li class="collection-item avatar">
                <img src="{{ asset('img/user.png') }}" alt="" class="circle">
                <span class="title">From: {{ $feedback->user->name }}</span>
                <p>                
                Feedback: {{ $feedback->feedback }}
                </p>
                <p>
                Score: {{ $feedback->score }}
                </p>
                <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
              </li>
            @endforeach
          </ul>
        </span>
    </div>
</div>
<!-- Modal Structure -->
<div id="modal1" class="modal">
  <div class="modal-content">
    <h4>Feedback</h4>
    <form method="POST" action=" {{ route('feedback',['storeid' => $store->id ] ) }}">
        <div class="row">
          <div class="input-field col s12">
            <textarea id="textarea1"  name="feedback" class="materialize-textarea" data-length="120"></textarea>
            <label for="textarea1">Feedback</label>
          </div>
          <div class="input-field col s12">
            <select name="score">
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
            </select>
            <label>Score</label>
          </div>
        </div>
        
        {{ csrf_field() }}
  </div>
    <div class="modal-footer">
      <button class="modal-action modal-close waves-effect waves-green btn-flat" id="send">Send</button>
      </form>
    </div>
</div>
@endsection
@push('scripts')
   <script>
     $(document).ready(function() {
        $('select').material_select();
      });
   </script>
@endpush