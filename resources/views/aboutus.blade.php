@extends('layouts.app')
@section('content')
<div class="container-modify" >
   <div class="row mt-2">
        <div class="col s12 m12">
            <div class="box-center">
                <img class="materialboxed" width="200" src="{{ asset('img/logo.png') }}">
            </div>
            <p class="flow-text" style="text-indent:3em">
            Intramuros is the main area of Manila where old Spanish-time impacts are plentiful. Fort Santiago is currently an all-around kept up and is still a popular tourist spot both for foreigners and locals.</p>
            <p  class="flow-text" style="text-indent:3em"> It is home to a population of 46,981, according to infomaninc, 2 universities, and 2 colleges. The usage of mobile applications increased steadily. Mobile applications start to play a bigger role in the lives of many Filipino. This application is showing various information of the food establishments in Intramuros.</p>
        </div>
    </div>
</div>
@endsection