@extends('layouts.app')
@section('content')
<div class="container-modify">
  <div class="row ">
    <div class="col s12 m6 mt-1">
      <div class="card">
        <div class="card-image">
          <img src="{{ asset('img/favoritz.jpg') }}">
          <span class="card-title">Luis-Thomas Canteen</span>
          <a class="btn-floating halfway-fab waves-effect waves-light yellow"><i class="material-icons">favorite</i></a>
        </div>
        <div class="card-content">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sed nibh fermentum, viverra quam sit amet, pharetra elit. Morbi tempus risus sed nulla porta fermentum. Nulla imperdiet id justo quis lobortis. Nulla ultrices cursus vulputate. Nunc tristique quam eget tristique lacinia</p>
        </div>
      </div>
    </div>
  </div>
    <div class="row">
    <div class="col s12 m6">
      <div class="card">
        <div class="card-image">
          <img src="{{ asset('img/favoritz.jpg') }}">
          <span class="card-title">Fang Mei</span>
          <a class="btn-floating halfway-fab waves-effect waves-light yellow"><i class="material-icons">favorite</i></a>
        </div>
        <div class="card-content">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sed nibh fermentum, viverra quam sit amet, pharetra elit. Morbi tempus risus sed nulla porta fermentum. Nulla imperdiet id justo quis lobortis. Nulla ultrices cursus vulputate. Nunc tristique quam eget tristique lacinia</p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection