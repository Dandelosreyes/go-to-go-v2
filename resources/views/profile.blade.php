@extends('layouts.app')
@section('content')
<div class="container-modify" >
   <div class="row mt-2">
       	<div class="col s12 m12">
       		<img src="img/profis.png" class="responsive-img" align="left" width="25%">
       		<h5 style="margin-top:1.25em;margin-left: 5em">{{ Auth::user()->name }}</h5>
       	</div>
   </div>
   <div class="row">
       	<div class="col s12 m12 box">
       		<div class="box-50">
       			<h5 class="text-center">Your Meals</h5>
       		</div>
       	</div>
       	<div class="col s12 m12 box" style="margin-top: .25em">
       		<div class="box-50">
       			<h5 class="text-center">Favorites</h5>
       		</div>
       	</div>
       	<div class="col s12 m12 box" style="margin-top: .25em">
       		<div class="box-50">
       			<h5 class="text-center">Location</h5>
       		</div>
       	</div>
       	<div class="col s12 m12 box" style="margin-top: .25em">
       		<div class="box-50">
       			<h5 class="text-center">Payment</h5>
       		</div>
       	</div>
   </div>
</div>
@endsection