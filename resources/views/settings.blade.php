@extends('layouts.app')
@section('content')
  <div class="container-modify" >
           <div class="row mt-2">
                <div class="col s12 m12">
                    <div class="col s4 m4">
                        <img src="img/profis.png" class="responsive-img" align="left" width="100%">
                    </div>
                    <div class="col s8 m8">
                        <p style="margin-top:1.25em;margin-left">
                            <h6>{{ Auth::user()->name }}</h6>
                            <h6>{{ Auth::user()->email }}</h6>
                        </p>
                    </div>
                </div>
           </div>
           <div class="row">
                <h5 class="text-center" style="width:90%;margin: 0 auto">Edit Favorites</h5>
           </div>
           <div class="row">
                <div class="col s12 m12 box">
                    <div class="box-50">
                        <h5 class="text-center">Hot Dishes</h5>
                    </div>
                </div>
                <div class="col s12 m12 box" style="margin-top: .25em">
                    <div class="box-50">
                        <h5 class="text-center">Soups & Noodles</h5>
                    </div>
                </div>
                <div class="col s12 m12 box" style="margin-top: .25em">
                    <div class="box-50">
                        <h5 class="text-center">Desserts</h5>
                    </div>
                </div>
                <div class="col s12 m12 box active-box" style="margin-top: .25em">
                    <div class="box-50 ">
                        <h5 class="text-center">Tea & Coffee</h5>
                    </div>
                </div>
                <div class="col s12 m12 box" style="margin-top: .25em">
                    <div class="box-50">
                        <h5 class="text-center">Drinks</h5>
                    </div>
                </div>
           </div>
           <div class="row">
                <h5 class="text-center" style="width:90%;margin: 0 auto">Events</h5>
                 <div class="col s12 m12 box" style="margin-top: .25em">
                    <div class="box-50">
                        <h5 class="text-center">Go to Calendar</h5>
                    </div>
                </div>
           </div>
           <div class="row">
               <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                <h5 class="text-center" style="width:90%;margin: 0 auto;color:red">Sign Out</h5>
                
                                        </a>
           </div>
        </div>
        <!--<li>-->
                     

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
@endsection