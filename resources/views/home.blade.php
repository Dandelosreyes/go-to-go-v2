@extends('layouts.app')
@section('content')
<div class="container-modify">
    <div class="row"> 
        <a href="{{ route('menu',['category' => 'hot-dishes']) }}" style="margin-top:1.5em;width:100%;color:black">
            <div class="col s4 m4">
                <img src="img/hotdishes.png"  class="responsive-img">
            </div>
            <div class="col s8 m8">
                <p><h5>Hot Dishes</h5>From rice meals, to main course, to heavy meals.</p>
            </div>
        </a>
    </div>
    <div class="row">
        <a href="{{ route('menu',['category' => 'soup']) }}" style="margin-top:1.5em;width:100%;color:black">
            <div class="col s4 m4">
                <img src="img/soup.png"  class="responsive-img">
            </div>
            <div class="col s8 m8">
                    <p><h5>Soups & Noodles</h5>Warm dishes served in broth and noodles.</p>
            </div>
        </a>
    </div>
    <div class="row">
        <a href="{{ route('menu',['category' => 'desserts']) }}" style="margin-top:1.5em;width:100%;color:black">
            <div class="col s4 m4">
                <img src="img/desserts.png"  class="responsive-img">
            </div>
            <div class="col s8 m8">
                    <p><h5>Desserts</h5>If you are a sweet tooth and you want some sugar rush. </p>
            </div>
        </a>
    </div>
    <div class="row">
        <a href="{{ route('menu',['category' => 'tea']) }}" style="margin-top:1.5em;width:100%;color:black">
            <div class="col s4 m4">
                <img src="img/coffee.png"  class="responsive-img">
            </div>
            <div class="col s8 m8">
                <p><h5>Tea & Coffee</h5>Something to chill with for a busy day </p>
            </div>
        </a>
    </div>
    <div class="row">
        <a href="{{ route('menu',['category' => 'drinks']) }}" style="margin-top:1.5em;width:100%;color:black">
            <div class="col s4 m4">
                <img src="img/drinks.png"  class="responsive-img">
            </div>
            <div class="col s8 m8">
                <p><h5>Drinks</h5>Night life after work? Get away from a busy. </p>
            </div>
        </a>
    </div>
</div>

@endsection
