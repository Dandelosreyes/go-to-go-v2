<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" >
    @stack('styles')
</head>
<body>
    <div id="app">
        @auth        
         <nav style="background-color:#CB3929 !important;" class="navshi"> 
            <div class="nav-wrapper navbar-fixed-top">
              <a href="#!" class="brand-logo">{{ $title }}</a>
              <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
              <ul class="side-nav" id="mobile-demo">
                <img src="{{ asset('img/rev/background.png') }}" class="responsive-img">
                <li class="{{ Nav::isRoute('home','active') }} {{ Nav::isRoute('menu','active') }} {{ Nav::isRoute('maps','active') }}">
                    <a href="{{ route('home') }}">
                        <img src="{{ asset('img/navbar/home.png') }}" class="nav-icons" width="70" align="left" style="float:left">Home
                    </a>
                </li>
                <li class="{{ Nav::isRoute('messages','active') }}">
                    <a href="{{ route('messages') }}">
                        <img src="{{ asset('img/navbar/msg.png') }}" class="nav-icons" width="70" align="left" style="float:left">Messages
                    </a>
                </li>
                <li class="{{ Nav::isRoute('profile','active') }}">
                    <a href="{{ route('profile') }}">
                        <img src="{{ asset('img/navbar/profile.png') }}" class="nav-icons" width="70" align="left" style="float:left">Profile
                    </a>
                </li>
                <li class="{{ Nav::isRoute('settings','active') }}">
                    <a href="{{ route('settings') }}">
                        <img src="{{ asset('img/navbar/settings.png') }}" class="nav-icons" width="70" align="left" style="float:left">Settings
                    </a>
                </li>
                <li class="{{ Nav::isRoute('favorites','active') }}">
                    <a href="{{ route('favorites') }}">
                        <img src="{{ asset('img/navbar/fav.png')}}" class="nav-icons" width="70" align="left" style="float:left">Favorites
                    </a>
                </li>
                <li class="{{ Nav::isRoute('aboutus','active') }}">
                    <a href="{{ route('aboutus') }}">
                        <img src="{{ asset('img/navbar/Go-to-Go-12.png' )}}" class="nav-icons" width="70" align="left" style="float:left">About Us
                    </a>
                </li>
                
              </ul>
            </div>
        </nav>

        @endauth

        @yield('content')
    </div>

    <!-- Scripts -->
    <script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/materialize.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/index.js') }}"></script>
    @stack('scripts')
</body>
</html>
