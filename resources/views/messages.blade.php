@extends('layouts.app')
@section('content')
<div class="container-modify">
   <ul class="collection">
        <li class="collection-item avatar">
          <img src="img/navbar/profile.png" alt="" class="circle">
          <span class="title">Denzel Becina</span>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin non aliquam leo, vitae fringilla felis.
          </p>
          <a href="#!" class="secondary-content"><i class="material-icons">markunread</i></a>
        </li>
        <li class="collection-item avatar">
          <img src="img/navbar/profile.png" alt="" class="circle">
          <span class="title">Xander Ford</span>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin non aliquam leo, vitae fringilla felis.
          </p>
          <a href="#!" class="secondary-content"><i class="material-icons">markunread</i></a>
        </li>
        <li class="collection-item avatar">
          <img src="img/navbar/profile.png" alt="" class="circle">
          <span class="title">Marlou Arizala</span>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin non aliquam leo, vitae fringilla felis.
          </p>
          <a href="#!" class="secondary-content"><i class="material-icons">markunread</i></a>
        </li>
        <li class="collection-item avatar">
          <img src="img/navbar/profile.png" alt="" class="circle">
          <span class="title">Empoy</span>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin non aliquam leo, vitae fringilla felis.
          </p>
        </li>
    </ul>
</div>
@endsection