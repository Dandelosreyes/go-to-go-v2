<?php

use Illuminate\Database\Seeder;
use App\Models\Store;
class StoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Store::create([
            "lat" => 14.590986,
	        "lng" => 129.9798709,
	        "name"=>"Angeli's Cuisine",
            "address"=>"Stall No 12 Muralla St, Bgy 658, Zone 070 Intramuros, Manila, Intramuros, Manila, Metro Manila",
            "tags"=>"lutong bahay, fried",
            "startprice"=>50,
            "endprice"=>80
        ]);
        Store::create([
            "lat"=> 14.591076,
            "lng"=> 120.978640,
            "name"=>"Fang Mei",
            "address"=>"Chamber 1 Puerta Isabel II Muralla St, Bgy 654, Zone 069 Intramuros, Manila, Intramuros, Manila, Metro Manila",
            "tags"=>"lutong bahay, fried",
            "startprice"=>50,
            "endprice"=>80
        ]);
        Store::create([
            "lat"=> 14.591174,
            "lng"=> 120.978610,
            "name"=>"Jazz Simple Ambiance Bar & Resto",
            "address"=>"Chamber 3 Puerta Isabel 2 Muralla St, Bgy 654, Zone 069 Intramuros, Manila, Intramuros, Manila, Metro Manila",
            "tags"=>"lutong bahay, fried",
            "startprice"=>50,
            "endprice"=>80        
        ]);
        Store::create([
            "lat"=> 14.591400, 
            "lng"=> 120.978535,
            "name"=>"Luis-Thomas Canteen",
            "address"=>"Stall 3 Muralla St, Bgy 658, Zone 070 Intramuros, Manila, Intramuros, Manila, Metro Manila",
            "tags"=>"lutong bahay, fried",
            "startprice"=>50,
            "endprice"=>80
        ]);
        Store::create([
            "lat"=> 14.591730, 
            "lng"=> 120.978318,
            "name"=>"Elenita Villar Kitchenet",
            "address"=>"2/F Stall 1 Student Center Bldg 151 Muralla St, Bgy 654, Zone 069 Intramuros, Manila, Intramuros, Manila, Metro Manila",
            "tags"=>"lutong bahay, fried",
            "startprice"=>50,
            "endprice"=>80
        ]);
        Store::create([
           "lat"=> 14.591916, 
           "lng"=> 120.978320,
           "name"=>"NATIONAL BURGER ADDICTION",
           "address"=>"Intramuros, Manila, Metro Manila",
           "tags"=>"burger, beef, meat",
           "startprice"=>50,
           "endprice"=>200 
        ]);
        Store::create([
            "lat"=> 14.591960, 
            "lng"=> 120.978181,
            "name"=>"Spicy Chicken",
            "address"=>"7 Muralla St, Intramuros, Manila, 1002 Metro Manila",
            "tags"=>"chicken, rice meal",
            "startprice"=>70,
            "endprice"=>90
        ]);
        Store::create([
            "lat"=> 14.592069, 
            "lng"=> 120.978099,
            "name"=>"Lorna Carinderia",
            "address"=>"STL 18 Muralla St, Intramuros, Manila, Intramuros, Manila, Metro Manila",
            "tags"=>"lutong bahay, fried, rice meal",
            "startprice"=>50,
            "endprice"=>80
        ]);
        Store::create([
            "lat"=> 14.591385, 
            "lng"=> 120.977330,
            "name"=>"Antonio Diola Restaurant",
            "address"=>"511 Legaspi St, Bgy 658, Zone 070 Intramuros Manila, Intramuros, Manila, Metro Manila",
            "tags"=>"lutong bahay, rice meal",
            "startprice"=>50,
            "endprice"=>80
        ]);
        Store::create([
            "lat"=> 14.591631, 
            "lng"=> 120.977228,
            "name"=>"Wapoo's Kitchen",
            "address"=>"774 Real St Cor Legaspi St, Bgy 658, Zone 070 Intramuros, Manila, Intramuros, Manila, Metro Manila",
            "tags"=>"lutong bahay, rice meal",
            "startprice"=>50,
            "endprice"=>80
        ]);
        Store::create([
            "lat"=>14.592720 , 
            "lng"=> 120.977832,
            "name"=>"McDonald's",
            "address"=>"1002, 61 Muralla St, Intramuros, Manila, 1002 Metro Manila",
            "tags"=>"fastfood, chicken, burger, fries",
            "startprice"=>60,
            "endprice"=>220
        ]);
        Store::create([
            "lat"=> 14.593079, 
            "lng"=> 120.978142,
            "name"=>"Kaga Donburi & Bento Japanese",
            "address"=>"Bldg 1 Asean Garden P Burgos St, Bgy 654, Zone 069 Intramuros Manila, Intramuros, Manila, Metro Manila",
            "tags"=>"Japanese, rice meal, chicken, ricebowls",
            "startprice"=>100,
            "endprice"=>200
        ]);
        Store::create([
            "lat"=> 14.593861, 
            "lng"=> 120.975332,
            "name"=>"Starbucks",
            "address"=>"Muralla, Intramuros, Manila, Metro Manila",
            "tags"=>"coffee, tea, cafe",
            "startprice"=>160,
            "endprice"=>220
        ]);
        Store::create([
            "lat"=> 14.593685, 
            "lng"=> 120.974510,
            "name"=>"Procopio",
            "address"=>"G/F Fermie Bldg Aduana, Intramuros, Manila, Intramuros, Manila, Metro Manila",
            "tags"=>"lutong bahay, rice meal",
            "startprice"=>80,
            "endprice"=>150
        ]);
        Store::create([
            "lat"=> 14.593805, 
            "lng"=> 120.973894,
            "name"=>"Jollibee",
            "address"=>"Muralla St, Intramuros, Manila, Metro Manila",
            "tags"=>"chicken, fast-food, pasta",
            "startprice"=>50,
            "endprice"=>200
        ]);
        Store::create([
            "lat"=>  14.593594,
            "lng"=>  120.973954,
            "name"=>"Max's Restaurant",
            "address"=>"Dante Ang Bldg, No. 409 A. Soriano Ave., 656 Zone 69, Intramuros, Manila, 1002 Metro Manila",
            "tags"=>"chicken, filipino food, casual dining",
            "startprice"=>160,
            "endprice"=>330
        ]);
        Store::create([
            "lat"=> 14.593318,  
            "lng"=> 120.973642,
            "name"=>"Chic - Boy",
            "address"=>"Dante Ang Bldg, No. 409 A. Soriano Ave, 656 Zone 69, Intramuros, Manila, 1002 Metro Manila",
            "tags"=>"rice meal, chicken, pork, unli-rice,unli rice",
            "startprice"=>120,
            "endprice"=>160
        ]);
        Store::create([
            "lat"=> 14.593246, 
            "lng"=> 120.973568,
            "name"=>"Bacolod Chicken House",
            "address"=>"Femii Bldg, A. Soriano Ave Intramuros, Brgy. 656 Zone 69, Manila, 1002 Metro Manila",
            "tags"=>"rice meal, chicken, pork, unli-rice, unli rice",
            "startprice"=>120,
            "endprice"=>160
        ]);
        Store::create([
            "lat"=> 14.592753, 
            "lng"=> 120.972951,
            "name"=>"KFC",
            "address"=>"Shipping Center Condo Bldg. Zone 69 1002, Soriano Ave, Intramuros, Manila, Metro Manila",
            "tags"=>"chicken, fast-food",
            "startprice"=>50,
            "endprice"=>200
        ]);
        Store::create([
            "lat"=>  14.587743, 
            "lng"=>  120.976742,
            "name"=>"Patio Victoria",
            "address"=>"5435 General Luna St, Intramuros, Manila, 1002 Metro Manila",
            "tags"=>"filipino food, buffet",
            "startprice"=>100,
            "endprice"=>200
        ]);
        Store::create([
            "lat"=> 14.587765,
            "lng"=> 120.977466,
            "name"=>"ILUSTRADO",
            "address"=>"744 General Luna St, Intramuros, Manila, Metro Manila",
            "tags"=>"filipino food, buffet, wine and beer",
            "startprice"=>150,
            "endprice"=>220
        ]);
        Store::create([
            "lat"=> 14.588446, 
            "lng"=> 120.977851,
            "name"=>"Faustina",
            "address"=>"Magallanes St, Intramuros, Manila, Metro Manila",
            "tags"=>"lutong bahay, fried, rice meal",
            "startprice"=>90,
            "endprice"=>150
        ]);
        Store::create([
            "lat"=> 14.589671, 
            "lng"=> 120.978609,
            "name"=>"Kitchen Basic",
            "address"=>"Victoria St, Intramuros, Manila, Metro Manila",
            "tags"=>"lutong bahay, fried, rice meal",
            "startprice"=>70,
            "endprice"=>100
        ]);
        Store::create([
            "lat"=> 14.589251, 
            "lng"=> 120.978180,
            "name"=>"7-Eleven",
            "address"=>"Solana Street Corner Victoria Streetintramuros, Metro Manila, Victoria St, Intramuros, Manila, Metro Manila",
            "tags"=>"fast food, beverages, convenience store",
            "startprice"=>50,
            "endprice"=>70
        ]);
        Store::create([
            "lat"=> 14.589206, 
            "lng"=> 120.978145,
            "name"=>"Moonleaf Tea Shop Intramuros",
            "address"=>"749 Victoria St, Intramuros, Manila, 1002 Metro Manila",
            "tags"=>"coffee, pastry, tea, pasta",
            "startprice"=>50,
            "endprice"=>200
        ]);
        Store::create([
            "lat"=> 14.589504, 
            "lng"=> 120.977959,
            "name"=>"Solana's",
            "address"=>"Solana St, Intramuros, Manila, Metro Manila",
            "tags"=>"lutong bahay, fried, rice meal",
            "startprice"=>50,
            "endprice"=>70
        ]);
        Store::create([
            "lat"=> 14.588579, 
            "lng"=> 120.978274,
            "name"=>"Café Pression",
            "address"=>"Escuela St, Intramuros, Manila, Metro Manila",
            "tags"=>"coffee, pastry, tea, pasta",
            "startprice"=>80,
            "endprice"=>20
        ]);
        Store::create([
            "lat"=> 14.588579, 
            "lng"=> 120.978274,
            "name"=>"Don Rodrigo's Cafe",
            "address"=>"Escuela St, Intramuros, Manila, Metro Manila",
            "tags"=>"coffee, pastry, tea, pasta",
            "startprice"=>80,
            "endprice"=>200
        ]);
        Store::create([
            "lat"=> 14.590461,
            "lng"=> 120.977142,
            "name"=>"Edlyn Eatery",
            "address"=>"531 Solana St.Intramuros, 658 Zone 70, Manila, 1002 Metro Manila",
            "tags"=>"ihaw, barbecue, lutong bahay, rice meal, pork, liempo",
            "startprice"=>40,
            "endprice"=>70
        ]);
        Store::create([
            "lat"=>  14.590718,
            "lng"=>  120.976910,
            "name"=>"Don Rodrigo's Cafe",
            "address"=>"Metro Manila, 613 Solana St, Intramuros, Manila, 1002 Metro Manila",
            "tags"=>"live music, full bar, nightlife, sandwich, bar food",
            "startprice"=>100,
            "endprice"=>200
        ]);
        Store::create([
            "lat"=>  14.591798,
            "lng"=>  120.974387,
            "name"=>"J's Sizzling House",
            "address"=>"670 Beaterio St, Intramuros, Manila, Metro Manila",
            "tags"=>"sizzling, hot plates, unli-rice, pork, chicken",
            "startprice"=>60,
            "endprice"=>100
        ]);
        Store::create([
            "lat"=>  14.593588,
            "lng"=>  120.978571,
            "name"=>"BEER GARDEN",
            "address"=>"Intramuros, Manila, Metro Manila",
            "tags"=>"beverages, beer, liquor, alcohol, snacks",
            "startprice"=>100,
            "endprice"=>200
        ]);
        Store::create([
            "lat"=> 14.590003,
            "lng"=> 120.975945,
            "name"=>"The Heritage Cafe & Restaurant",
            "address"=>"Urdaneta St, Intramuros, 1002 Metro Manila",
            "tags"=>"casual dining, breakfast, foreign cuisine",
            "startprice"=>200,
            "endprice"=>700
        ]);
        Store::create([
            "lat"=> 14.589926, 
            "lng"=> 120.978769,
            "name"=>"Sky Deck View Bar, The Bayleaf Intramuros",
            "address"=>"Top of The Bayleaf Intramuros, Muralla corner Victoria streets, Intramuros, Manila, 1002 Metro Manila",
            "tags"=>"bar, casual dining, full bar, nightlife",
            "startprice"=>150,
            "endprice"=>600
        ]);
        Store::create([
            "lat"=> 14.589255, 
            "lng"=> 120.975442,
            "name"=>"Barbara's Casa Manila",
            "address"=>"Plaza San Luis Complex, Gen. Luna Street, Intramuros, Intramuros, Manila, 1002 Metro Manila",
            "tags"=>"buffet, full bar, casual dining",
            "startprice"=>200,
            "endprice"=>700
        ]);
        Store::create([
            "lat"=> 14.589278,
            "lng"=> 120.974521,
            "name"=>"Ristorante delle Mitre",
            "address"=>"Opposite San Agustin Church, Real St, Intramuros, Manila, Metro Manila",
            "tags"=>"casual dining, breakfast, wine and beer, outdoor seating",
            "startprice"=>200,
            "endprice"=>700
        ]);
        Store::create([
            "lat"=> 14.591472,
            "lng"=> 120.974291,
            "name"=>"La Casteilaya Cafe",
            "address"=>"1002, Cabildo Street, Corner Beaterio Street, Intramuros, Intramuros, Manila, 1002 Metro Manila",
            "tags"=>"coffee, pastry, tea",
            "startprice"=>80,
            "endprice"=>200
        ]);
        Store::create([
            "lat"=> 14.591472,
            "lng"=> 120.974291,
            "name"=>"Patio de Conchita",
            "address"=>"Beaterio St, Intramuros, Manila, Metro Manila",
            "tags"=>"lutong bahay, beer, alcohol, bar, rice meal",
            "startprice"=>100,
            "endprice"=>200
        ]);
        Store::create([
            "lat"=> 14.591683,
            "lng"=> 120.974162,
            "name"=>"Cafe Janealo",
            "address"=>"670 Beaterio St, Intramuros, Manila, Metro Manila",
            "tags"=>"coffee, pastry, tea, breakfast",
            "startprice"=>100,
            "endprice"=>300
        ]);
        Store::create([
            "lat"=> 14.588274,
            "lng"=> 120.978628,
            "name"=>"Manila Bulletin",
            "address"=>"Muralla St, Intramuros, Manila, 1002 Metro Manila",
            "tags"=>"lutong bahay, rice meal",
            "startprice"=>59,
            "endprice"=>300
        ]);
        Store::create([
            "lat"=> 14.588744,
            "lng"=> 120.978784,
            "name"=>"Paper + Cup Coffee",
            "address"=>"Muralla St, Intramuros, Manila, 1002 Metro Manila",
            "tags"=>"coffee, pastry, tea",
            "startprice"=>100,
            "endprice"=>300
        ]);
    }
}
