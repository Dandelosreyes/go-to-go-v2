<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Kevin Tud',
            'email' => 'user@example.com',
            'password' => '123'
        ]);
        User::create([
            'name' => "You",
            'email' => 'test@example.com',
            'password' => '123'
        ]);
    }
}
