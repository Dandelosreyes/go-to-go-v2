<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Store;
use Mapper;
use App\Models\Feedback;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home')->with('title','CRAVING FOR?');
    }
    public function menu($menu)
    {
        if($menu == 'hot-dishes')
        {
            $data = Store::where('tags','LIKE' ,"%". 'bahay' ."%")
                    ->orWhere('tags','LIKE' ,"%". 'rice' ."%")
                    ->orWhere('tags','LIKE' ,"%". 'fried' ."%")
                    ->orWhere('tags','LIKE',"%". 'breakfast' ."%")
                    ->orWhere('tags','LIKE',"%".'sizzling'."%")
                    ->orWhere('tags','LIKE',"%".'unli-rice'."%")
                    ->orWhere('tags','LIKE',"%".'casual dining'."%")
                    ->get()->toJson();
            return view('menu')->with(['data' => $data , 'title' => 'HOT DISHES']);
        }
        elseif($menu == 'soup')
        {
            $data =  Store::where('tags','LIKE' ,"%". 'pasta' ."%")
                    ->orWhere('tags','LIKE' ,"%". 'pastry' ."%")
                    ->get();
            return view('menu')->with(['data' => $data , 'title' => 'HOT DISHES']);
        }
        elseif($menu == 'desserts')
        {
            $data =  Store::where('tags','LIKE' ,"%". 'pasta' ."%")
                    ->orWhere('tags','LIKE' ,"%". 'coffee' ."%")
                    ->orWhere('tags','LIKE' ,"%". 'tea' ."%")
                    ->get();
            return view('menu')->with(['data' => $data , 'title' => 'DESSERTS']);
        }
        elseif($menu == 'tea'){
            $data =  Store::where('tags','LIKE' ,"%". 'coffee' ."%")
                    ->orWhere('tags','LIKE' ,"%". 'tea' ."%")
                    ->orWhere('tags','LIKE',"%".'night life'."%")
                    ->get();
          return view('menu')->with(['data' => $data , 'title' => 'Tea & Coffee']); 
        }
        elseif($menu == 'drinks')
        {
           $data = Store::where('tags','LIKE' ,"%". 'beer' ."%")
                    ->orWhere('tags','LIKE' ,"%". 'alcohol' ."%")
                    ->orWhere('tags','LIKE',"%".'night life'."%")
                    ->orWhere('tags','LIKE',"%".'bar'."%")
                    ->orWhere('tags','LIKE',"%".'wine and beer'."%")
                    ->get();
            return view('menu')->with(['data' => $data , 'title' => 'Drinks']);
        }
    }
    public function map($id)
    {
        $feedback = Feedback::where('store_id','=',$id)->with(['user','store'])->get();
        $store = Store::find($id);
        Mapper::map($store->lat, $store->lng ,['zoom' => 30, 'center' => true, 'marker' => true ]);
        return view('maps')->with(['store' => $store , 'feedbacks' => $feedback ,'title' => $store->name]);
    }
    public function feedback(Request $request, $id)
    {
        $feedback = new Feedback;
        $feedback->user_id = Auth::user()->id;
        $feedback->store_id = $id;
        $feedback->feedback = $request->feedback;
        $feedback->score = $request->score;
        $feedback->save();
        return redirect()->back();
    }
    public function register(Request $request)
    {
        dd($request->all());
    }
}
