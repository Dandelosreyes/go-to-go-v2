<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
    public function store()
    {
        return $this->belongsTo('App\Models\Store','store_id');
    }
}
